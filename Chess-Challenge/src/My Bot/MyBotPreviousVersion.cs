﻿using ChessChallenge.API;
using System;
using System.Runtime.CompilerServices;

public class MyBotPreviousVersion : IChessBot
{
    int[] pieceValues = { 0, 100, 200, 250, 500, 900, 1000 };
    int maxTimeUsage, infinity = 100000, checkmate = 50000, mobilityWeight = 2;
    const byte INVALID = 0, EXACT = 1, LOWERBOUND = 2, UPPERBOUND = 3;
    static ulong k_TpMask = 0x7FFFFF, nodes = 0;
    Transposition[] m_TPTable = new Transposition[k_TpMask + 1];
    struct Transposition
    {
        public ulong zobristHash;
        public Move move;
        public int evaluation;
        public sbyte depth;
        public byte flag;
    };

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    int EvaluateMaterial(Board board, bool isWhiteToMove)
    {
        return BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Pawn, isWhiteToMove)) * pieceValues[1] +
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Knight, isWhiteToMove)) * pieceValues[2] +
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Bishop, isWhiteToMove)) * pieceValues[3] +
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Rook, isWhiteToMove)) * pieceValues[4] +
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Queen, isWhiteToMove)) * pieceValues[5] -
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Pawn, !isWhiteToMove)) * pieceValues[1] -
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Knight, !isWhiteToMove)) * pieceValues[2] -
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Bishop, !isWhiteToMove)) * pieceValues[3] -
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Rook, !isWhiteToMove)) * pieceValues[4] -
                BitboardHelper.GetNumberOfSetBits(board.GetPieceBitboard(PieceType.Queen, !isWhiteToMove)) * pieceValues[5];
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    int EvaluateMobility(Board board)
    {
        Span<Move> moves = stackalloc Move[300];
        board.GetLegalMovesNonAlloc(ref moves);

        return moves.Length * mobilityWeight;
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    int Evaluate(Board board)
    {
        int score = EvaluateMobility(board) + EvaluateMaterial(board, board.IsWhiteToMove);

        board.ForceSkipTurn();
        score += -EvaluateMobility(board);
        board.UndoSkipTurn();

        return score;
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    string GetMoveNameUCI(Move move)
    {
        if (move.IsNull)
            return "Null";

        char[] startSquareName = { 'a', '1' };
        startSquareName[0] += (char)move.StartSquare.File;
        startSquareName[1] += (char)move.StartSquare.Rank;
        char[] endSquareName = { 'a', '1' };
        endSquareName[0] += (char)move.TargetSquare.File;
        endSquareName[1] += (char)move.TargetSquare.Rank;

        string moveName = new string(startSquareName) + new string(endSquareName);
        if (move.IsPromotion)
        {
            switch (move.PromotionPieceType)
            {
                case PieceType.Rook:
                    moveName += "r";
                    break;
                case PieceType.Knight:
                    moveName += "n";
                    break;
                case PieceType.Bishop:
                    moveName += "b";
                    break;
                case PieceType.Queen:
                    moveName += "q";
                    break;
            }
        }
        return moveName;
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    int Search(Board board, Move currentMove, Timer timer, int alpha, int beta, int depth)
    {
        nodes++;

        if (board.IsInCheckmate())
            return -checkmate - depth;
        else if (board.IsDraw())
            return 0;
        else if (timer.MillisecondsElapsedThisTurn > maxTimeUsage)
            return alpha;

        int lowerBound = alpha;

        ulong zKey = board.ZobristKey;

        ref Transposition tp = ref m_TPTable[zKey & k_TpMask];

        bool qSearch = depth <= 0;

        if (tp.move == currentMove && tp.zobristHash == zKey && tp.depth >= depth)
        {
            if (tp.flag == EXACT)
                return tp.evaluation;
            else if (tp.flag == UPPERBOUND)
            {
                if (tp.evaluation >= beta)
                    return beta;
                else
                    beta = tp.evaluation;
            }
            else if (tp.flag == LOWERBOUND && tp.evaluation > alpha)
                lowerBound = alpha = tp.evaluation;
        }

        if (qSearch)
        {
            int stand_pat = Evaluate(board);

            if (stand_pat >= beta)
                return beta;

            if (alpha < stand_pat)
                alpha = stand_pat;
        }

        tp.flag = INVALID;
        tp.depth = (sbyte)depth;
        tp.move = currentMove;
        tp.zobristHash = zKey;

        Span<Move> moves = stackalloc Move[300];
        board.GetLegalMovesNonAlloc(ref moves, qSearch);


        foreach (Move move in moves)
        {
            if (move.IsNull) break;

            board.MakeMove(move);
            int score = -Search(board, move, timer, -beta, -alpha, depth - 1);
            board.UndoMove(move);

            if (score >= beta)
            {
                tp.flag = UPPERBOUND;
                tp.evaluation = beta;
                return beta;
            }

            alpha = Math.Max(alpha, score);

            if (timer.MillisecondsElapsedThisTurn > maxTimeUsage)
                break;
        }

        tp.flag = alpha == lowerBound ? LOWERBOUND : EXACT;
        tp.evaluation = alpha;
        return alpha;
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    public Move Think(Board board, Timer timer)
    {
        int depth = 1, timeStarted = timer.MillisecondsElapsedThisTurn;
        maxTimeUsage = (int)(timer.MillisecondsRemaining * 0.05f + timer.IncrementMilliseconds * 0.90f);

        Span<Move> moves = stackalloc Move[300];
        board.GetLegalMovesNonAlloc(ref moves);

        Move bestMove = moves[0];
        Move prevBestMove = bestMove;

        do
        {
            int bestScore = -infinity;
            nodes = 0;

            foreach (Move move in moves)
            {
                board.MakeMove(move);
                int score = -Search(board, move, timer, -infinity, infinity, depth);
                board.UndoMove(move);

                if (score > bestScore)
                {
                    bestScore = score;
                    bestMove = move;
                }
            }

            if (timer.MillisecondsElapsedThisTurn < maxTimeUsage)
            {
                Console.WriteLine("info depth {0} score cp {1} nodes {3} nps {4} pv {2}", depth, bestScore, GetMoveNameUCI(bestMove), nodes, nodes * 1000ul / (ulong)Math.Max(1, timer.MillisecondsElapsedThisTurn - timeStarted));
                prevBestMove = bestMove;
            }

            depth++;

        }
        while (timer.MillisecondsElapsedThisTurn < maxTimeUsage);

        return prevBestMove;
    }
}