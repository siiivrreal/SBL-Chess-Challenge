# C# Chess Engine
Chess Engine Originally made for Sebastian Lague's chess challenge. (Original readme in CHALLENGE.MD)

I ran out of steam after one week and the code was getting unreadable so I didn't submit it, but I'm still working on it.

**This project is currently being slow cooked.**

## Features
- [UCI Support (Click for repo)](https://github.com/GediminasMasaitis/Chess-Challenge-Uci/tree/uci)
- [Transposition tables](https://www.chessprogramming.org/Transposition_Table)
- [Negamax search framework](https://www.chessprogramming.org/Negamax)
- [Alpha Beta Search](https://www.chessprogramming.org/Alpha-Beta)
- [Quiescence search](https://www.chessprogramming.org/Quiescence_Search)
- A brain dead evaluation function featuring:
  - [Material Balance](https://www.chessprogramming.org/Material)
  - [Mobility](https://www.chessprogramming.org/Mobility)

## Known issues
- Quiescence search tanks search depth
- Bot sometimes blunders due to low depth and other low depth issues

*A special thanks to Sebastian Lague for inspiring myself and many others to try chess engine programming*